interface Fish {
  name1: string
}
interface Water {
  name2: string
}
interface Bird {
  name3: string
}
interface Sky {
  name4: string
}
type Condition<T> = T extends Fish ? Water : Bird;
let con1: Condition<Fish> = { name2: '水' }
let con1_2: Condition<Sky> = { name3: '鸟' }

let con2: Condition<Fish|Bird> = { name2: '水' };
let con2_2: Condition<Fish|Bird> = { name3: 'bird' };

type Exclude1<T, U> = T extends U ? never : T;
type myExclude = Exclude1<'1' | '2' | '3' | '4', '1' | '2' | '5'>; // 用每一个去进行判断的
let con3: myExclude = '3';

type Extract1<T, U> = T extends U ? T : never;
type MyExtract = Extract1<'1' | '2' | '3', '1' | '2'>
let con4: MyExtract = '1';

type NonNullable1<T> = T extends null | undefined ? never : T
type MyNone = NonNullable1<'a' | null | undefined>
let con5: MyNone = 'a';

var obj = {
  a: '1',
  b: '2',
  c: function(){console.log('c')}
}
const { a, c } = obj
// 相当于：
// a = obj.a // '1'
// c = obj.c // function(){console.log('c')}

class Person1 {
  constructor(name: string, age: number) { }
}
type InstanceType1<T> = T extends { new(...args: any): infer R } ? R : any
type MyInstance = InstanceType1<typeof Person1>

// 联合类型：将数组类型转化为联合类型
type ElementOf<T> = T extends Array<infer E> ? E : never;
type TupleToUnion = ElementOf<[string, number, boolean]>;

// 交叉类型：将两个函数的参数转化为交叉类型
type T1 = { name: string };
type T2 = { age: number };
type ToIntersection<T> = T extends ([(x: infer U) => any, (x: infer U) => any]) ? U : never;
type t3 = ToIntersection<[(x:T1)=>any,(x:T2)=>any]>

interface Person1 {
  handsome: string,
}
interface Person24 {
  high: string,
}
type P1P2 = Person1 & Person24;
let p: P1P2 = { handsome: '帅', high: '高' }

