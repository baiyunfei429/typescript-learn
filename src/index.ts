let person1 = {
  name:'zf',
  age:11
}
type Person2 = typeof person1
console.log(typeof person1)

interface String {
  double():string
}
String.prototype.double = function () {
  return this as string + this + '999';
}
let str = 'zhufeng';
console.log(str)
console.log(str.double())

interface Window {
  mynane:string
}
console.log(window.mynane)

class Form {}
namespace Form {
    export const type = 'form'
}
let form1: Form = new Form()

function getName(){}
namespace getName {
    export const type = 'form'
}

enum Seasons {
  Spring = 'Spring',
  Summer = 'Summer'
}
namespace Seasons{
  export let Autum = 'Autum';
  export let Winter = 'Winter'
}