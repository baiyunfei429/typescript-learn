export default 'cunstom'

let person = {
  name: 'zhufeng',
  age: 11,
  address: '回龙观'
}
let person22 = {
  address: '回龙观',
}
type Diff<T extends Object, K extends Object> = Omit<T, keyof K>
type DiffPerson = Diff<typeof person, typeof person22>


type InterSection1<T extends Object, K extends Object> = Pick<T, Extract<keyof T, keyof K>>
type InterSectionPerson = InterSection1<typeof person, typeof person22>

type OldProps = { name: string, age: number, visible: boolean };
type NewProps = { age: string, other: string };

type Compute<A extends any> = { [K in keyof A]: A[K] };
type Merge<T, K> = Compute<Omit<T, keyof K> & K>;
type MergeObj = Merge<OldProps,NewProps>

type key = keyof unknown;
type IMap<T> = {
  [P in keyof T]: T[P]
}
type t = IMap<typeof person22>;
