interface Company {
  num: number
}
interface Person {
  name: string,
  age: string,
  company: Company
}
type Partial1<T> = {[K in keyof T]?: T[K]};
type PartialPerson = Partial<Person>;

type DeepPartial<T> = {
  [K in keyof T]?: T[K] extends object ? DeepPartial<T[K]> : T[K];
}
type DeepPartialPerson = DeepPartial<Person>


type Required1<T> = {[K in keyof T]-?:T[K]} 
type RequiredPerson = Required1<PartialPerson>

type ToPartial<T> = { readonly [K in keyof T] ?: T[K] }
type Ds = ToPartial<Person>

type Pick1<T, U extends keyof T> = { [P in U]: T[P] }
type PickPerson = Pick1<Person, 'name' | 'age'>

type Record1<K extends keyof any, T> = { [P in K]  : T }
let person3: Record1<string, any> = { name: 'zf', age: 11 };

let personOmit = {
  name: 'string',
  age: 'string',
  company: {},
  address: '天'
}
type Omit1<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
type OmitAddress = Omit1<typeof personOmit, 'address'>


type InferArrayType<T> = T extends (infer U)[] ? U : never;
type FirstArrayType<T> = T extends [infer P, ... infer _] ? P : never;
type LastArrayType<T> = T extends [... infer _, infer P] ? P : never;
type InferArray1 = InferArrayType<[1, 3]>
type FirstArrayType1 = FirstArrayType<[1, 2, 3]>
type LastArrayType1 = LastArrayType<[1, 3, 5, string]>

type InferParameters<T> = T extends (...args: infer R) => any ? R : never;
type InferReturnParameters<T> = T extends (...args: any) => infer R ? R : never;
type IS = InferParameters<(args1: string, args2: number)=>any>
type IS1 = InferReturnParameters<(args1: string, args2: number)=>number>

type InferPromise<T> = T extends Promise<infer R> ? R : never;
type InferPromise1 = InferPromise<Promise<string>>

type InferFirstString<T> = T extends `${infer First}${infer _}` ? First : []
type I8 = InferFirstString<'Baiyunfei'>

